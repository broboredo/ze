var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    //Compilar o foundation
    mix.sass('base.scss', 'public/css');
    mix.sass('main.scss', 'public/css');
    mix.sass('admin/base.scss', 'public/css/admin');
    mix.sass('admin/main.scss', 'public/css/admin');
    mix.sass('normalize.scss', 'public/css');
});
