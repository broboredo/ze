# ZéEncontra.com

Repositório do site zeencontra.com

## Instalação

1. Renomeie o arquivo ".ev.example" para .env e o configure com os dados do servidor.

2. `Composer update` para baixar as dependências em PHP.

3. `npm install` para baixar as dependências Javascript. Caso dê erro de conexão, utilize o comando `npm config delete proxy` antes.

## DB

Migrate: criando as tabelas do banco ($ php artisan migrate)
Migrate: atualizando tabelas ($ php artisan migrate:refresh)

COM AS TABELAS CRIADAS:
	Seed: Minerando dados ($ php artisan db:seed --class="NomeDoSeed")
		UserSeed (para criar usuário)
		CategorySeed (para criar categorias E subcategorias)