<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seller_name', 255);
            $table->string('email', 255);
            $table->string('company_name', 255);
            $table->string('trading_name', 255);
            $table->string('cnpj', 20);
            $table->string('site', 120);
            $table->string('phone', 15);
            $table->string('cellphone', 15);
            $table->string('postal_code', 12);
            $table->string('district', 150);
            $table->string('city', 150);
            $table->string('state', 80);
            $table->string('street', 255);
            $table->integer('number');
            $table->string('complement', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stores');
    }
}
