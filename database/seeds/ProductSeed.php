<?php

use Illuminate\Database\Seeder;
use App\Http\Controllers\ProductController;

class ProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$category = new ProductController();
    	$category->install();
    }
}
