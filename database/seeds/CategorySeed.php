<?php

use Illuminate\Database\Seeder;
use App\Http\Controllers\CategoryController;

class CategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$category = new CategoryController();
    	$category->install();
    }
}
