$( document ).ready(function() {
    
    //JS DO FOUNDATION
	$(document).foundation();

	//SCROLL BAR
	$('.menu').mCustomScrollbar({
		theme: "3d-thick",
		scrollInertia: 500 //delay do scroll em milisegundos
	});

	//GRAFICO NUMERO DE PRODUTOS
	$('.chart').easyPieChart({
		barColor: '#ea3f24',
		trackColor: '#e9eff1',
		lineWidth: 10,
		size: 200
	});

});

