$( document ).ready(function() {
    
    //JS DO FOUNDATION
	$(document).foundation();

	
	$('.navegue').click(function() {
	    $('html, body').animate({
	        scrollTop: $('#categorias').offset().top
	    }, 1000);
	});

	$('#categorias ul li').click(function() {
		if(!$(this).hasClass('ativo')){

			

			// initialize the selectize control
var $select = $(this).find('.select').selectize();

// fetch the instance
var selectize = $select[0].selectize;

			var alturaBox = $(this).height()-30;
			var larguraBox = $(this).width();

			var altura = $("#categorias ul li form").height();

			$('.box').width(larguraBox).height(alturaBox);
			$('#categorias ul li').addClass('some').fadeOut(500);

			var ativo = $(this);
			setTimeout(function(){
				$(ativo).addClass('ativo').fadeIn(1000).height('100%');
				
			}, 500);
		}
	});

	$('#categorias .voltar').click(function(e) {
		e.stopPropagation();

		$('select').each(function() {
		    if (this.selectize) {
		        this.selectize.destroy();
		    }
		});

		$('#categorias ul li').removeClass('ativo some').removeAttr('style');
		$('.box').removeAttr('style');
	});


});