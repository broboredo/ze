<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ZéEncontra.com - Administração</title>

  <link href="{{ asset('/css/normalize.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/admin/base.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/admin/main.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/admin/jquery.mCustomScrollbar.css') }}" rel="stylesheet">

</head>
<body>
	<div class="large-3 medium-4 columns menu">
    <div class="row topo">
      <a href="/" target="_blank">
        <img src="{{ asset('/img/logo-admin.png') }}" alt="ZéEncontra.com - O melhor preço, sempre!">
      </a>

      <a href="javascript:;" class="icon-menu right"></a>
    </div>

    <div class="row notificacoes">
      <span class="icon-bell"></span>
      <h4>Notificações <span>3</span></h4>

      <ul>
        <li>Novo produto publicado com sucesso. <br><a href="javascript:;">Clique aqui</a> para visualizar o anúncio.</li>
        <li>Novo produto publicado com sucesso. <br><a href="javascript:;">Clique aqui</a> para visualizar o anúncio.</li>
        <li>Novo produto publicado com sucesso. <br><a href="javascript:;">Clique aqui</a> para visualizar o anúncio.</li>
      </ul>
    </div>
    <div class="clearfix"></div>

    <div class="row ultimos-anuncios">
      <span class="icon-megaphone"></span>
      <h4>Últimos Anúncios</h4>

      <ul>
        <li class="disabled">
          <div class="txt left">
            <p>Ultrabook Samsung...</p>
            <p class="desc">Exynos 5 Dual Core</p>
          </div>

          <div class="acao right">
            <span class="status"></span>
            <span class="icon-pencil"></span>
            <span class="icon-cancel-circled"></span>
          </div>
        </li>

        <li>
          <div class="txt left">
            <p>Ultrabook Samsung...</p>
            <p class="desc">Exynos 5 Dual Core</p>
          </div>

          <div class="acao right">
            <span class="status"></span>
            <span class="icon-pencil"></span>
            <span class="icon-cancel-circled"></span>
          </div>
        </li>

        <li class="disabled">
          <div class="txt left">
            <p>Ultrabook Samsung...</p>
            <p class="desc">Exynos 5 Dual Core</p>
          </div>

          <div class="acao right">
            <span class="status"></span>
            <span class="icon-pencil"></span>
            <span class="icon-cancel-circled"></span>
          </div>
        </li>

        <li class="disabled">
          <div class="txt left">
            <p>Ultrabook Samsung...</p>
            <p class="desc">Exynos 5 Dual Core</p>
          </div>

          <div class="acao right">
            <span class="status"></span>
            <span class="icon-pencil"></span>
            <span class="icon-cancel-circled"></span>
          </div>
        </li>

        <li>
          <div class="txt left">
            <p>Ultrabook Samsung...</p>
            <p class="desc">Exynos 5 Dual Core</p>
          </div>

          <div class="acao right">
            <span class="status"></span>
            <span class="icon-pencil"></span>
            <span class="icon-cancel-circled"></span>
          </div>
        </li>

        <li>
          <div class="txt left">
            <p>Ultrabook Samsung...</p>
            <p class="desc">Exynos 5 Dual Core</p>
          </div>

          <div class="acao right">
            <span class="status"></span>
            <span class="icon-pencil"></span>
            <span class="icon-cancel-circled"></span>
          </div>
        </li>
      </ul>
    </div>
    <div class="clearfix"></div>
  </div>

  <div class="large-9 medium-8 columns right" id="conteudo">
    <div class="large-2 columns config right">
      <span class="icon-cog right dropdown" role="button" data-options="align:left" data-dropdown="config" aria-controls="config" aria-expanded="false"></span>
      <ul id="config" class="medium f-dropdown" data-dropdown-content  aria-hidden="true">
        <li>Meus Ajustes</li>
        <li><a href="javascript:;"><span class="icon-user"></span> Meu cadastro</a></li>
        <li><a href="javascript:;"><span class="icon-bag"></span> Minha loja</a></li>
        <li><a href="javascript:;"><span class="icon-tools"></span> Configurações</a></li>
        <li><a href="javascript:;"><span class="icon-logout"></span> Sair</a></li>
      </ul>
    </div>

    @yield('content')

    <footer class="right">
        <p>ZéEncontra.com | v0.1</p>
    </footer>  
  </div>
  
  <script src="{{ asset('/js/vendor/jquery.js') }}"></script>
  <script src="{{ asset('/js/vendor/fastclick.js') }}"></script>
  <script src="{{ asset('/js/foundation.min.js') }}"></script>
  <script src="{{ asset('/js/vendor/jquery.mCustomScrollbar.js') }}"></script>
  <script src="{{ asset('/js/vendor/jquery.easypiechart.js') }}"></script>
  <script src="{{ asset('/js/admin.js') }}"></script>
</body>
</html>
