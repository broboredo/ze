@extends('template/internal')

@section('content')
	<div class="title">
		<h2>{!! $title !!}</h2>
	</div>
	<iframe src="https://player.vimeo.com/video/122159832?color=ea3f25&title=0&byline=0&portrait=0" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

	<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspend</p>

	<div class="title">
		<h2 class="cmm">Faça parte da <strong>comunidade</strong></h2>
	</div>

	<div class="comunidade">
		<div class="large-6 columns">
			<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos</p>
		
			<ul>
				<li><a class="button" href="javascript:;">Anuncie grátis</a></li>
				<li><a class="button inverse" href="javascript:;">Cadastre-se grátis</a></li>
			</ul>
		</div>
		<div class="large-6 columns">
			<img src="{{ asset('img/comunidade.png') }}" alt="Comunidade">
		</div>
	</div>
@stop