<footer>
		<div class="compartilhe">
			<div class="row">
				<p>Compartilhe</p>
				<ul>
					<li><a class="addthis_button_facebook facebook"></a></li>
                	<li><a class="addthis_button_twitter twitter"></a></li>
                	<li><a class="addthis_button_compact mais"></a></li>
				</ul>
				<p>Redes Sociais</p>
				<ul>
					<li><a href="https://www.facebook.com/ZeEncontra" target="_blank" class="facebook"><span></span></a></li>
                	<li><a href="http://twitter.com/Ze_Encontra" target="_blank" class="twitter"><span></span></a></li>
                	<li><a href="javascript:;" target="_blank" class="instagram"><span></span></a></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns logo-rodape">
				<img src="{{ asset('/img/logo-rodape.png') }}" alt="Zé Encontra.com">
			</div>

			<div class="large-8 columns texto">
				<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>
			
				<hr>

				<nav>
					<ul class="large-4 columns">
						<li>O Zé Encontra</li>
						<li><a href="<?=url();?>/sobre">Sobre</a></li>
						<li><a href="<?=url();?>/contato">Fale com o Zé</a></li>
						<li><a href="<?=url();?>/faq">Perguntas Frequentes</a></li>
						<li><a href="<?=url();?>/seus-direitos">Seus direitos</a></li>
					</ul>

					<ul class="large-4 columns">
						<li>Anunciantes</li>
						<li><a href="<?=url();?>/anunciante">Área do Anunciante</a></li>
						<li><a class="button" href="<?=url();?>/anuncie">Anuncie Grátis</a></li>
					</ul>

					<ul class="large-4 columns">
						<li>Legal</li>
						<li><a href="#" data-reveal-id="termos">Termos de Uso</a></li>
						<li><a href="<?=url();?>/politica-de-privacidade">Política de Privacidade</a></li>
						<li><a href="<?=url();?>/contato">Entre em Contato Conosco</a></li>
					</ul>
				</nav>

				<hr>
				<div class="clearfix"></div>
				<p><a href="<?=url();?>/contato">Não encontrou o que procurava? Faça uma sugestão.</a></p>
			</div>
		</div>
	</footer>

<!-- TERMOS -->

<div id="termos" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <div class="title">
  	<h2>Termos de Uso</h2>
  </div>
  
  <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim. This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>

  <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim. This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>

  <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>

  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>