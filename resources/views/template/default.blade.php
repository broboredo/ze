<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="{{ asset('/img/favicon.png') }}" type="image/x-icon">
	<title>Zé Encontra.com - O melhor preço sempre!</title>

	<link href="{{ asset('/css/normalize.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/base.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/selectize.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/main.css') }}" rel="stylesheet">
</head>
<body>
	<section id="topo">
		<header>
			<h1>
				<a href="<?=url();?>/"><img class="center" src="{{ asset('img/logo-home.png') }}" alt="Zé Encontra.com - O melhor preço sempre!"></a>
			</h1>
		</header>

		<h2><span><strong>Milhares de produtos</strong> em lojas</span> <span>garantidas por <strong>você, consumidor.</strong></span></h2>	
	
		<div class="large-9 columns center">
			<form>
				<div class="large-9 columns">
					<input type="text" class="search" id="search" placeholder="Deixe o Zé encontrar o que você procura...">
				</div>
				<div class="large-3 columns">
					<input type="submit" class="find" value="Encontrar">
				</div>

				<div class="clearfix"></div>
			</form>
		</div>

		<div class="clearfix"></div>

		<div class="large-4 columns center">
			<a class="navegue" href="javascript:;">Navegue pelas categorias <span></span></a>
		</div>
	</section>

	@yield('content')

	@include('template.footer')

	<script src="{{ asset('/js/vendor/jquery.js') }}"></script>
	<script src="{{ asset('/js/foundation.min.js') }}"></script>
	<script src="{{ asset('/js/vendor/addthis.js') }}"></script>
	<script src="{{ asset('/js/vendor/selectize.js') }}"></script>
	<script src="{{ asset('/js/main.js') }}"></script>
</body>
</html>