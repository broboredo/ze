<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="{{ $description }}">
	<link rel="shortcut icon" href="{{ asset('/img/favicon.png') }}" type="image/x-icon">
	<title>{{ $name }} | Zé Encontra.com</title>

	<link href="{{ asset('/css/normalize.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/base.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/main.css') }}" rel="stylesheet">
</head>
<body class="internal {{ $class }}">
	<section id="topo">
		<header class="row">
			<div class="large-4 columns">
				<h1>
					<a href="<?=url();?>/"><img class="center" src="{{ asset('img/logo-home.png') }}" alt="Zé Encontra.com - O melhor preço sempre!"></a>
				</h1>
			</div>
			
			<form class="large-6 columns">
				<div class="large-9 columns">
					<input type="text" class="search" id="search" placeholder="Deixe o Zé encontrar o que você procura...">
				</div>
				<div class="large-3 columns">
					<input type="submit" class="find" value="Encontrar">
				</div>
			</form>

			<div class="large-2 columns">
				<a class="menu"><span></span> Menu</a>
			</div>
			
		</header>
	</section>

	<section class="row" id="conteudo">
		@yield('content')
	</section>

	@include('template.footer')

	<script src="{{ asset('/js/vendor/jquery.js') }}"></script>
	<script src="{{ asset('/js/vendor/addthis.js') }}"></script>
	<script src="{{ asset('/js/foundation.min.js') }}"></script>
	<script src="{{ asset('/js/main.js') }}"></script>
</body>
</html>