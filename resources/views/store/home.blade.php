@extends('admin')

@section('content')
<div class="large-10 columns">
      <h2 class="left">Olá <strong>Pedro</strong>, o que deseja fazer agora?</h2>

      <button class="button radius right">Inserir anúncio</button>

      <hr>

    </div>

    <div class="clearfix"></div>
    
    <div class="large-12 columns">
      <h3 class="destaque">Estatísticas Gerais</h3>
    </div>

    <div class="row">
      <div class="large-4 columns">
        <div class="box">
          <div class="chart" data-percent="86">
            <span class="percent"><span>86</span>/100<br>produtos ativos</span>
          </div>
        </div>
      </div>

      <div class="large-8 columns">
        <div class="box">
          <h5>Seus produtos mais vistos</h5>
          <p class="em-breve">Em breve!</p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="large-4 columns">
        <h3>Marketing</h3>
        <div class="box">
          <p class="em-breve">Em breve!</p>
        </div>
      </div>

      <div class="large-4 columns">
        <h3>Financeiro</h3>
        <div class="box">
          <p class="em-breve">Em breve!</p>
        </div>
      </div>

      <div class="large-4 columns">
        <h3>Fale com o Zé</h3>
        <div class="box">
          <a href="javascript:;">Clique aqui e fale conosco para que possamos te ajudar.</a>
        </div>
      </div>
    </div>
@endsection