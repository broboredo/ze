@extends('template/internal')

@section('content')
	<div class="title">
		<h2>{!! $title !!}</h2>
	</div>

	<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac</p>

	<div class="large-6 columns">
		<img class="fale" src="{{ asset('img/fale.png') }}" alt="Fale com o Zé">
	</div>

	<div class="large-6 columns">
		@if($errors->all())
			<ul class="large-12 columns errors">
				<h3>@if(count($errors->all()) > 1) Erros @else Erro @endif ao enviar formulário:</h3>
				@foreach($errors->all() as $error)
					<li class="large-12 error">{{ $error }}</li>
		    	@endforeach
		    </ul>
	    @endif
	    
	    @if(Session::has('success'))
 			<h3 class="large-12 columns success">{{ Session::get('success') }}</h3>
		@endif
	    
    	
		{!! Form::open(array('url' => 'contato/enviar')) !!}
			<div class="row">
				<div class="large-12 columns">
					{!! Form::text('name', null, array('required' => 'required', 'placeholder' => 'Qual seu nome?')) !!}
				</div>
		    	
		    	<div class="large-12 columns">
		    		{!! Form::text('email', null, array('required' => 'required', 'placeholder' => 'Qual seu email?')) !!}
				</div>

				<div class="large-12 columns">
					<div class="select">
						{!! Form::select('subject', ['duvida' => 'Dúvida', 'reclamacao' => 'Reclamação', 'sugestao' => 'Sugestão'], null, ['required' => 'required', 'placeholder' => 'Selecione o assunto']) !!}
					</div>
				</div>
				
				<div class="large-12 columns">
					{!! Form::textarea('message', null, array('required' => 'required', 'placeholder' => 'Mande sua mensagem para o Zé')) !!}
					{!! Form::submit('Enviar', array('class' => 'right')) !!}
				</div>
			</div>
		{!! Form::close() !!}
	</div>
@stop