@extends('template.default')

@section('content')
<section id="categorias">
		<h3><span>Selecione uma <strong>categoria</strong> abaixo</span> <span>e <strong>encontre</strong> o que você procura</span></h3>
		<ul class="row">
			<li class="large-3 columns">
				<div class="box">
					<span class="computadores"></span> Computadores<br> e Notebooks
				</div>

				<div class="info">
					<a href="javascript:;" class="voltar">Voltar</a>	

					<!-- CARREGAR DINAMICAMENTE -->
					<form>
						<select class="select fabricante" placeholder="Digite ou selecione o fabricante">
							<option value="">Digite ou selecione o fabricante</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
							<option value="fsdfdsadfs">Roboredo</option>
							<option value="marfsadcio">Marcio</option>
							<option value="jodssdao">João</option>
							<option value="robsddfsdoredo">Roboredo</option>
							<option value="dsfsdfs">Marcio</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsffddfcvcvsdfsdao">João</option>
							<option value="jodsfsddfdffsdao">João</option>
							<option value="jodsfsddfffsdao">João</option>
							<option value="jodsfsdfdffdsdao">João</option>
							<option value="jodsfsdddfdddsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdffcsdfsdao">João</option>
							<option value="jodsfsdcvcvvfsdao">João</option>
							<option value="jodsfscvcvdfsdao">João</option>
							<option value="jodsfsvcvcdfsdao">João</option>
							<option value="jodsfscvcdfsdao">João</option>
						</select>

						<select class="select modelo" placeholder="Digite ou selecione o modelo">
							<option value="">Digite ou selecione o modelo</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>

						<select class="select demo-default loja" placeholder="Digite ou selecione o local da loja">
							<option value="">Digite ou selecione o local da loja</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>
					</form>

					<div class="clearfix"></div>

					<!-- <p class="recentes">Veja os mais <strong>recentes</strong></p> -->
				</div>
			</li>

			<li class="large-3 columns">
				<div class="box">
					<span class="dispositivos"></span> Dispositivos<br> e Impressoras
				</div>

				<div class="info">
					<a href="javascript:;" class="voltar">Voltar</a>	

					<!-- CARREGAR DINAMICAMENTE -->
					<form>
						<select class="select fabricante" placeholder="Digite ou selecione o fabricante">
							<option value="">Digite ou selecione o fabricante</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
							<option value="fsdfdsadfs">Roboredo</option>
							<option value="marfsadcio">Marcio</option>
							<option value="jodssdao">João</option>
							<option value="robsddfsdoredo">Roboredo</option>
							<option value="dsfsdfs">Marcio</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsffddfcvcvsdfsdao">João</option>
							<option value="jodsfsddfdffsdao">João</option>
							<option value="jodsfsddfffsdao">João</option>
							<option value="jodsfsdfdffdsdao">João</option>
							<option value="jodsfsdddfdddsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdffcsdfsdao">João</option>
							<option value="jodsfsdcvcvvfsdao">João</option>
							<option value="jodsfscvcvdfsdao">João</option>
							<option value="jodsfsvcvcdfsdao">João</option>
							<option value="jodsfscvcdfsdao">João</option>
						</select>

						<select class="select modelo" placeholder="Digite ou selecione o modelo">
							<option value="">Digite ou selecione o modelo</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>

						<select class="select demo-default loja" placeholder="Digite ou selecione o local da loja">
							<option value="">Digite ou selecione o local da loja</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>
					</form>

					<div class="clearfix"></div>

					<!-- <p class="recentes">Veja os mais <strong>recentes</strong></p> -->
				</div>
			</li>

			<li class="large-3 columns">
				<div class="box">
					<span class="celulares"></span> Celulares<br> e Tablets
				</div>

				<div class="info">
					<a href="javascript:;" class="voltar">Voltar</a>	

					<!-- CARREGAR DINAMICAMENTE -->
					<form>
						<select class="select fabricante" placeholder="Digite ou selecione o fabricante">
							<option value="">Digite ou selecione o fabricante</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
							<option value="fsdfdsadfs">Roboredo</option>
							<option value="marfsadcio">Marcio</option>
							<option value="jodssdao">João</option>
							<option value="robsddfsdoredo">Roboredo</option>
							<option value="dsfsdfs">Marcio</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsffddfcvcvsdfsdao">João</option>
							<option value="jodsfsddfdffsdao">João</option>
							<option value="jodsfsddfffsdao">João</option>
							<option value="jodsfsdfdffdsdao">João</option>
							<option value="jodsfsdddfdddsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdffcsdfsdao">João</option>
							<option value="jodsfsdcvcvvfsdao">João</option>
							<option value="jodsfscvcvdfsdao">João</option>
							<option value="jodsfsvcvcdfsdao">João</option>
							<option value="jodsfscvcdfsdao">João</option>
						</select>

						<select class="select modelo" placeholder="Digite ou selecione o modelo">
							<option value="">Digite ou selecione o modelo</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>

						<select class="select demo-default loja" placeholder="Digite ou selecione o local da loja">
							<option value="">Digite ou selecione o local da loja</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>
					</form>

					<div class="clearfix"></div>

					<!-- <p class="recentes">Veja os mais <strong>recentes</strong></p> -->
				</div>
			</li>

			<li class="large-3 columns">
				<div class="box">
					<span class="videogames"></span> Videogames<br> e Consoles
				</div>

				<div class="info">
					<a href="javascript:;" class="voltar">Voltar</a>	

					<!-- CARREGAR DINAMICAMENTE -->
					<form>
						<select class="select fabricante" placeholder="Digite ou selecione o fabricante">
							<option value="">Digite ou selecione o fabricante</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
							<option value="fsdfdsadfs">Roboredo</option>
							<option value="marfsadcio">Marcio</option>
							<option value="jodssdao">João</option>
							<option value="robsddfsdoredo">Roboredo</option>
							<option value="dsfsdfs">Marcio</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsffddfcvcvsdfsdao">João</option>
							<option value="jodsfsddfdffsdao">João</option>
							<option value="jodsfsddfffsdao">João</option>
							<option value="jodsfsdfdffdsdao">João</option>
							<option value="jodsfsdddfdddsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdffcsdfsdao">João</option>
							<option value="jodsfsdcvcvvfsdao">João</option>
							<option value="jodsfscvcvdfsdao">João</option>
							<option value="jodsfsvcvcdfsdao">João</option>
							<option value="jodsfscvcdfsdao">João</option>
						</select>

						<select class="select modelo" placeholder="Digite ou selecione o modelo">
							<option value="">Digite ou selecione o modelo</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>

						<select class="select demo-default loja" placeholder="Digite ou selecione o local da loja">
							<option value="">Digite ou selecione o local da loja</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>
					</form>

					<div class="clearfix"></div>

					<!-- <p class="recentes">Veja os mais <strong>recentes</strong></p> -->
				</div>
			</li>

			<li class="large-3 columns">
				<div class="box">
					<span class="multimidia"></span> Multimídia
				</div>

				<div class="info">
					<a href="javascript:;" class="voltar">Voltar</a>	

					<!-- CARREGAR DINAMICAMENTE -->
					<form>
						<select class="select fabricante" placeholder="Digite ou selecione o fabricante">
							<option value="">Digite ou selecione o fabricante</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
							<option value="fsdfdsadfs">Roboredo</option>
							<option value="marfsadcio">Marcio</option>
							<option value="jodssdao">João</option>
							<option value="robsddfsdoredo">Roboredo</option>
							<option value="dsfsdfs">Marcio</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsffddfcvcvsdfsdao">João</option>
							<option value="jodsfsddfdffsdao">João</option>
							<option value="jodsfsddfffsdao">João</option>
							<option value="jodsfsdfdffdsdao">João</option>
							<option value="jodsfsdddfdddsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdffcsdfsdao">João</option>
							<option value="jodsfsdcvcvvfsdao">João</option>
							<option value="jodsfscvcvdfsdao">João</option>
							<option value="jodsfsvcvcdfsdao">João</option>
							<option value="jodsfscvcdfsdao">João</option>
						</select>

						<select class="select modelo" placeholder="Digite ou selecione o modelo">
							<option value="">Digite ou selecione o modelo</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>

						<select class="select demo-default loja" placeholder="Digite ou selecione o local da loja">
							<option value="">Digite ou selecione o local da loja</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>
					</form>

					<div class="clearfix"></div>

					<!-- <p class="recentes">Veja os mais <strong>recentes</strong></p> -->
				</div>
			</li>

			<li class="large-3 columns">
				<div class="box">
					<span class="softwares"></span> Softwares
				</div>

				<div class="info">
					<a href="javascript:;" class="voltar">Voltar</a>	

					<!-- CARREGAR DINAMICAMENTE -->
					<form>
						<select class="select fabricante" placeholder="Digite ou selecione o fabricante">
							<option value="">Digite ou selecione o fabricante</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
							<option value="fsdfdsadfs">Roboredo</option>
							<option value="marfsadcio">Marcio</option>
							<option value="jodssdao">João</option>
							<option value="robsddfsdoredo">Roboredo</option>
							<option value="dsfsdfs">Marcio</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsffddfcvcvsdfsdao">João</option>
							<option value="jodsfsddfdffsdao">João</option>
							<option value="jodsfsddfffsdao">João</option>
							<option value="jodsfsdfdffdsdao">João</option>
							<option value="jodsfsdddfdddsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdffcsdfsdao">João</option>
							<option value="jodsfsdcvcvvfsdao">João</option>
							<option value="jodsfscvcvdfsdao">João</option>
							<option value="jodsfsvcvcdfsdao">João</option>
							<option value="jodsfscvcdfsdao">João</option>
						</select>

						<select class="select modelo" placeholder="Digite ou selecione o modelo">
							<option value="">Digite ou selecione o modelo</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>

						<select class="select demo-default loja" placeholder="Digite ou selecione o local da loja">
							<option value="">Digite ou selecione o local da loja</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>
					</form>

					<div class="clearfix"></div>

					<!-- <p class="recentes">Veja os mais <strong>recentes</strong></p> -->
				</div>
			</li>

			<li class="large-3 columns">
				<div class="box">
					<span class="redes"></span> Redes
				</div>

				<div class="info">
					<a href="javascript:;" class="voltar">Voltar</a>	

					<!-- CARREGAR DINAMICAMENTE -->
					<form>
						<select class="select fabricante" placeholder="Digite ou selecione o fabricante">
							<option value="">Digite ou selecione o fabricante</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
							<option value="fsdfdsadfs">Roboredo</option>
							<option value="marfsadcio">Marcio</option>
							<option value="jodssdao">João</option>
							<option value="robsddfsdoredo">Roboredo</option>
							<option value="dsfsdfs">Marcio</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsffddfcvcvsdfsdao">João</option>
							<option value="jodsfsddfdffsdao">João</option>
							<option value="jodsfsddfffsdao">João</option>
							<option value="jodsfsdfdffdsdao">João</option>
							<option value="jodsfsdddfdddsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdffcsdfsdao">João</option>
							<option value="jodsfsdcvcvvfsdao">João</option>
							<option value="jodsfscvcvdfsdao">João</option>
							<option value="jodsfsvcvcdfsdao">João</option>
							<option value="jodsfscvcdfsdao">João</option>
						</select>

						<select class="select modelo" placeholder="Digite ou selecione o modelo">
							<option value="">Digite ou selecione o modelo</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>

						<select class="select demo-default loja" placeholder="Digite ou selecione o local da loja">
							<option value="">Digite ou selecione o local da loja</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>
					</form>

					<div class="clearfix"></div>

					<!-- <p class="recentes">Veja os mais <strong>recentes</strong></p> -->
				</div>
			</li>

			<li class="large-3 columns">
				<div class="box">
					<span class="outros"></span> Outros
				</div>

				<div class="info">
					<a href="javascript:;" class="voltar">Voltar</a>	

					<!-- CARREGAR DINAMICAMENTE -->
					<form>
						<select class="select fabricante" placeholder="Digite ou selecione o fabricante">
							<option value="">Digite ou selecione o fabricante</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
							<option value="fsdfdsadfs">Roboredo</option>
							<option value="marfsadcio">Marcio</option>
							<option value="jodssdao">João</option>
							<option value="robsddfsdoredo">Roboredo</option>
							<option value="dsfsdfs">Marcio</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsfsdfsdao">João</option>
							<option value="jodsffddfcvcvsdfsdao">João</option>
							<option value="jodsfsddfdffsdao">João</option>
							<option value="jodsfsddfffsdao">João</option>
							<option value="jodsfsdfdffdsdao">João</option>
							<option value="jodsfsdddfdddsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdddsdfsdao">João</option>
							<option value="jodsfdffcsdfsdao">João</option>
							<option value="jodsfsdcvcvvfsdao">João</option>
							<option value="jodsfscvcvdfsdao">João</option>
							<option value="jodsfsvcvcdfsdao">João</option>
							<option value="jodsfscvcdfsdao">João</option>
						</select>

						<select class="select modelo" placeholder="Digite ou selecione o modelo">
							<option value="">Digite ou selecione o modelo</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>

						<select class="select demo-default loja" placeholder="Digite ou selecione o local da loja">
							<option value="">Digite ou selecione o local da loja</option>
							<option value="rafael">Lorem Ipsum dolor sit amet</option>
							<option value="roboredo">Roboredo</option>
							<option value="marcio">Marcio</option>
							<option value="joao">João</option>
						</select>
					</form>

					<div class="clearfix"></div>

					<!-- <p class="recentes">Veja os mais <strong>recentes</strong></p> -->
				</div>
			</li>
		</ul>
	</section>

	<section id="como-funciona">
		<h4><span>Veja como o <strong>Zé Encontra</strong> funciona</span></h4>

		<div class="row">
			<div class="large-6 columns">
				<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed</p>
			
				<label for="nome">Assine nossa newsletter</label>
				
				{!! Form::open(array('url' => 'newsletter/adicionar')) !!}
					{!! Form::text('name', null, array('required' => 'required', 'placeholder' => 'Digite seu nome')) !!}
					{!! Form::text('email', null, array('required' => 'required', 'placeholder' => 'Digite seu email')) !!}
					{!! Form::submit('OK') !!}
					<div class="clearfix"></div>
				{!! Form::close() !!}
				
				@if(Session::has('success'))
		 			<h3 class="large-12 success newsletter">{{ Session::get('success') }}</h3>
				@endif
				
				@if($errors->all())
					<ul class="large-12 errors">
						<h3>@if(count($errors->all()) > 1) Erros @else Erro @endif ao enviar formulário:</h3>
						@foreach($errors->all() as $error)
							<li class="large-12 error">{{ $error }}</li>
		    			@endforeach
		    		</ul>
	   			@endif
			</div>

			<div class="large-6 columns">
				<iframe width="100%" height="400" src="https://www.youtube.com/embed/HzaFJqVER4w?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</section>
	@stop