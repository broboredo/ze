@extends('template/internal')

@section('content')
	<div class="title">
		<h2>{!! $title !!}</h2>
	</div>

	<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac</p>

  <ul class="accordion" data-accordion>
    <li class="accordion-navigation active">
      <a href="#faq1">Pergunta 1 ?</a>
      
      <div id="faq1" class="content active">
        <p>Panel 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      </div>
    </li>
  
    <li class="accordion-navigation">
      <a href="#faq2">Pergunta 2 ?</a>
      
      <div id="faq2" class="content">
        <p>Panel 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      </div>
    </li>

    <li class="accordion-navigation">
      <a href="#faq3">Pergunta 3 ?</a>
      
      <div id="faq3" class="content">
        <p>Panel 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      </div>
    </li>
  </ul>

  <div class="title">
    <h3>Não encontrou a resposta que precisava?</h3>

    <a class="button" href="<?=url();?>/contato">Fale com o Zé</a>
  </div>
@stop