<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
    	return [
    			'name' => 'required|max:125',
    			'email' => 'required|email|max:255',
    			'subject' => 'required',
    			'message' => 'required|min:5',
    	];
    }
    
    public function messages()
    {
    	return [
    			'name.required' => 'O campo Nome é obrigatório',
    			'name.max' => 'O campo Nome não pode ter mais que 125 caracteres',
    			'email.required' => 'O campo Email é obrigatório',
    			'email.email' => 'O campo Email precisa ser um email válido',
    			'email.max' => 'O campo Email não pode ter mais que 255 caracteres',
    			'message.required' => 'O campo Mensagem não pode ficar vazio',
    			'message.min' => 'O campo Mensagem não pode ter menos que 5 caracteres',
    	];
    }
    
}
