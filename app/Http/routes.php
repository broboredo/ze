<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PageController@index');

Route::get('home', 'PageController@index');

//INTERNALS
Route::get('sobre', 'PageController@about');
Route::get('contato', ['as' => 'contact', 'uses' => 'PageController@contact']);
Route::get('faq', 'PageController@faq');
Route::get('seus-direitos', 'PageController@your_rights');
Route::get('politica-de-privacidade', 'PageController@political_privacy');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/abc', 'ProductController@relationsManuf');

/* ADMIN LOJISTA */
//Route::get('admin', 'StoreController@home');
Route::get('/admin', ['middleware' => 'auth', 'uses' => 'StoreController@home']); //rota protegida middleware

Route::get('auth/login', ['as' => 'auth.form', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@getLogout']);

//Atualizando categoria/subcategoria/produtos
Route::get('/admin/db/category/refresh', ['middleware' => 'auth', 'uses' => 'CategoryController@install']);
Route::get('/admin/db/subcategory/refresh', ['middleware' => 'auth', 'uses' => 'SubcategoryController@install']);
Route::get('/admin/db/manufacturer/refresh', ['middleware' => 'auth', 'uses' => 'ManufacturerController@install']);
Route::get('/admin/db/product/refresh', ['middleware' => 'auth', 'uses' => 'ProductController@install']);

//Todos formas de contato
Route::post('/contato/enviar', ['uses' => 'ContactsController@contactSend']);
Route::post('/newsletter/adicionar', ['as' => 'newsletter', 'uses' => 'ContactsController@newsletterAdd']);
