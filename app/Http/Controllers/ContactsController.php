<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ContactFormRequest;
use App\Contact;

use App\Http\Requests\NewsletterFormRequest;
use App\Newsletter;

class ContactsController extends Controller
{
    public function contactSend(ContactFormRequest $request, Contact $contato) {    	
    	$contato->name = $request->get('name');
    	$contato->email = $request->get('email');
    	$contato->subject = $request->get('subject');
    	$contato->message = $request->get('message');
    	
    	$contato->save();
    	
    	return \Redirect::route('contact')->with('success', 'Mensagem enviada com sucesso!');
	}
	
	public function newsletterAdd(NewsletterFormRequest $request, Newsletter $newsletter) {    	
    	$newsletter->name = $request->get('name');
    	$newsletter->email = $request->get('email');
    	
    	$newsletter->save();
    	
    	return redirect()->back()->with('success', 'Você está agora cadastrado em nossa newsletter');
		
	}
}
