<?php namespace App\Http\Controllers;

class PageController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}

	/**
	 * Página sobre
	 *
	 * @return Response
	 */
	public function about()
	{
		return view('about', [
								'name' => 'Sobre', //Nome que aparecerá no <title>
								'class' => 'about', //Classe que será adicionada no body para fácil manipulação no CSS
								'title' => 'Conheça o <strong>Zé Encontra</strong>', //Texto que aparecerá no título principal da página (fundo azul)
								'description' => 'Descrição da página!', //Descrição SEO
							]);
	}

	/**
	 * Página contato
	 *
	 * @return Response
	 */
	public function contact()
	{
		return view('contact', [
								'name' => 'Fale com o Zé', //Nome que aparecerá no <title>
								'class' => 'contact', //Classe que será adicionada no body para fácil manipulação no CSS
								'title' => 'Fale com o <strong>Zé</strong>', //Texto que aparecerá no título principal da página (fundo azul)
								'description' => 'Descrição da página!', //Descrição SEO
							]);
	}

	/**
	 * Página faq
	 *
	 * @return Response
	 */
	public function faq()
	{
		return view('faq', [
								'name' => 'Perguntas frequentes', //Nome que aparecerá no <title>
								'class' => 'faq', //Classe que será adicionada no body para fácil manipulação no CSS
								'title' => 'Perguntas frequentes pro <strong>Zé</strong>', //Texto que aparecerá no título principal da página (fundo azul)
								'description' => 'Descrição da página!', //Descrição SEO
							]);
	}

	/**
	 * Página seus direitos
	 *
	 * @return Response
	 */
	public function your_rights()
	{
		return view('your_rights', [
								'name' => 'Seus direitos', //Nome que aparecerá no <title>
								'class' => 'your-rights', //Classe que será adicionada no body para fácil manipulação no CSS
								'title' => 'Seus <strong>Direitos</strong>', //Texto que aparecerá no título principal da página (fundo azul)
								'description' => 'Descrição da página!', //Descrição SEO
							]);
	}

	/**
	 * Página seus direitos
	 *
	 * @return Response
	 */
	public function political_privacy()
	{
		return view('political_privacy', [
								'name' => 'Política de Privacidade', //Nome que aparecerá no <title>
								'class' => 'political-privacy', //Classe que será adicionada no body para fácil manipulação no CSS
								'title' => 'Política de <strong>Privacidade</strong>', //Texto que aparecerá no título principal da página (fundo azul)
								'description' => 'Descrição da página!', //Descrição SEO
							]);
	}

}
