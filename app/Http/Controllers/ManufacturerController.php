<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\Manufacturer;

class ManufacturerController extends Controller
{
	public function install() {
		$prods = DB::table('products')
		->join('subcategories', function ($join) {
			$join->on('products.subcategory_id', '=', 'subcategories.id');
		})
		->join('categories', function ($joinon) {
			$joinon->on('subcategories.category_id', '=', 'categories.id')
			->where('categories.enable', '=', 1);
		})
		->select('products.name')
		->get();
	
		foreach ($prods as $p) {
			$manuf = $p->name;
			$manuf = explode(' ',trim($manuf));
			$manuf = utf8_decode($manuf[0]);
			
			$hasManuf = DB::table('manufacturers')
			->where('name', '=', $manuf)
			->select()
			->get();
			
			if(!empty($hasManuf)) {
				continue;
			}
			
			$itemManuf = new Manufacturer();
			$itemManuf->name = $manuf;
			$itemManuf->save();
		}		 
	}
}
