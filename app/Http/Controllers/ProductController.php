<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Subcategory;
use App\Product;
use DB;

class ProductController extends Controller
{
    public function install() {
    	$subcategories = DB::table('subcategories')
					    	->join('categories', function ($join) {
					    		$join->on('subcategories.category_id', '=', 'categories.id')
					    		->where('categories.enable', '=', 1);
					    	})
					    	->select('subcategories.id')
					    	->get();
					    	
    	ini_set('max_execution_time', 80000);
    	
    	foreach ($subcategories as $s) {
			$obj = file_get_contents('http://sandbox.buscape.com.br/service/findProductList/lomadee/7a4d5362452f2b6f4a32343d/?categoryId='.$s->id.'&format=json');
			$obj = json_decode($obj);
			if(!property_exists($obj, 'product'))
				continue;
			$obj = $obj->product;
			
			foreach ($obj as $o) {
				if(!$o)
					continue;
				
				$full_description = !$o->product->fulldescription ? '' : $o->product->fulldescription;
				$maxprice = !isset($o->product->pricemax) ? floatval(0) : floatval($o->product->pricemax);
				$minprice = !isset($o->product->pricemin) ? floatval(0) : floatval($o->product->pricemin);
				$thumb = (!isset($o->product->thumbnail) || !isset($o->product->thumbnail->url)) ? '' :$o->product->thumbnail->url;
				
				$thisProduct = new Product();
				$thisProduct->id = $o->product->id;
				$thisProduct->name = $o->product->productname;
				$thisProduct->subcategory_id = $o->product->categoryid;
				$thisProduct->med_price = (($maxprice+$minprice)/2);
				$thisProduct->full_description = $full_description;
				$thisProduct->default_image = $thumb;
				$thisProduct->save();
			}
    	}
    }
    
    public function relationsManuf() {
    	$prods = DB::table('products')->select()->get();
    	
    	foreach ($prods as $prod) {
    		$manuf = $prod->name;
    		$manuf = explode(' ',trim($manuf));
    		$manuf = utf8_decode($manuf[0]);
    		
    	
    		$manufs = DB::table('manufacturers')
    		->where('manufacturers.name', '=', $manuf)
    		->select()
    		->get();


    		$products = DB::table('products')
    		->where('id', $prod->id)
    		->update(['manufacturer_id' => $manufs[0]->id]);
    	}
    	echo 'FIM';
    }
    
}
