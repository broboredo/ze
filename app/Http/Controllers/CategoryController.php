<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
	public function install() {
		$obj = file_get_contents('http://sandbox.buscape.com.br/service/findCategoryList/lomadee/7a4d5362452f2b6f4a32343d/?categoryId=0&format=json');
		$obj = json_decode($obj);
		$obj = $obj->subcategory;
		
		foreach ($obj as $o) {
			if(!$o)
				continue;
			
			$thisCategory = new Category();	
			$thisCategory->id = $o->subcategory->id;
			$thisCategory->name = $o->subcategory->name;
			$thisCategory->save();
		}
		
		//instalando subcategorias
		$subcategory = new SubcategoryController();
		$subcategory->install();
	}
}
