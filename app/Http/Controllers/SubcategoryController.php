<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Subcategory;
use App\Category;

class SubcategoryController extends Controller
{
    public function install() {
    	$categories = Category;
    	
    	ini_set('max_execution_time', 600);
    	
    	foreach ($categories as $c) {
			$obj = file_get_contents('http://sandbox.buscape.com.br/service/findCategoryList/lomadee/7a4d5362452f2b6f4a32343d/?categoryId='.$c->id.'&format=json');
			$obj = json_decode($obj);
			if(!property_exists($obj, 'subcategory'))
				continue;
			$obj = $obj->subcategory;
			
			foreach ($obj as $o) {
				if(!$o)
					continue;
					
					$thisSubcategory = new Subcategory();
					$thisSubcategory->id = $o->subcategory->id;
					$thisSubcategory->name = $o->subcategory->name;
					$thisSubcategory->category_id = $o->subcategory->parentcategoryid;
					$thisSubcategory->save();
			}			
    	}
    }
}
