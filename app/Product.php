<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [
			'name',
			'subcategory_id',
			'manufacturer_id',
			'full_description',
			'med_price',
			'default_image',
			'created_at',
			'updated_at'
	];

	public function subcategory() {
		return $this->belongsTo('App\Subcategory');
	}
	
	public function manufacturer() {
		return $this->belongsTo('App\Manufacturer');
	}
}
