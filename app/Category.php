<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = [
			'name',
			'created_at',
			'updated_at'
	];
	
	public function subcategories() {
		return $this->hasMany('App\Subcategory');
	}
}
